//
// Created by diediaz on 29-06-18.
//

#ifndef STRINGFINGERPRINTGENERATOR_PRIMEGENERATOR_HPP
#define STRINGFINGERPRINTGENERATOR_PRIMEGENERATOR_HPP

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <random>
#include "Prng.hpp"
#include "ModOperations.hpp"

class PrimeGenerator {
private:
    typedef unsigned long long int size_type;
public:
    inline static size_type getRandomPrime(size_t min_range, size_t max_range){
        //k = number of iterations
        size_type k=4, prime_candidate=2;
        std::random_device rd;
        Prng prng(rd());
        Prng prng_2(rd());

        max_range = max_range - min_range +1;

        //prime_candidate must be an odd number
        while(!(prime_candidate & 1U)){
            prime_candidate = min_range + prng.rand()%max_range;
        }

        while(!millerRabinPrimTest(prime_candidate, k, prng_2)){
            prime_candidate = min_range + prng.rand()%max_range;
            while(!(prime_candidate & 1U)){
                prime_candidate = min_range + prng.rand()%max_range;
            }
        };
        return prime_candidate;
    };

    inline static bool witness(size_t a, size_t n){
        size_t t,u,x, prev_x;

        size_t tmp,pos =0;
        tmp = n-1;
        while(!(tmp & (1U<<pos))){
            pos++;
        }
        u = tmp >> pos;
        t = pos;

        x = ModOperations::mod_pow(a,u,n);
        prev_x = x;

        for(size_t i=0;i<t;i++){
            x = ModOperations::mod_pow(x ,2,n);
            if(x==1 && prev_x!=1 && prev_x != tmp){
                return true;
            }
            prev_x = x;
        }
        return x != 1;
    };

    inline static bool millerRabinPrimTest(size_t n, size_t s, Prng& prng){
        size_t a;
        size_t tmp = n-1;

        for(size_t j=0;j<s;j++){
            a = 1 + prng.rand()%tmp;
            if(witness(a, n)){
                return false;
            }
        }
        return true;
    };
};


#endif //STRINGFINGERPRINTGENERATOR_PRIMEGENERATOR_HPP
