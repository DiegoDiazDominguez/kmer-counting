//
// Created by diediaz on 12-07-18.
//

#include "DNAFingerPrint.hpp"

DNAFingerPrint::DNAFingerPrint(std::string &fq_file, size_t kmer_size, size_t app_dollars):
        fqr(fq_file, FastQReader::FORWARD, app_dollars) {

    m_k_size = kmer_size;

    m_dollar_diff = (int)(m_k_size - fqr.append_dollars);
    m_tot_kmers = (uint32_t)(fqr.n_chars - fqr.n_reads*(m_k_size-1) + (fqr.n_reads-1)*fqr.append_dollars);
    m_n_printed_kmers = 0;

    m_curr_read_kmer = 0;
    m_curr_read_length = ((size_t) fqr.curr_read_len) + 2*fqr.append_dollars;

    //generate a random prime number m_q in the range [min_range, max_range], where m_q * max(d, d**-1) must fit a machine word
    auto min_range = (size_t)ceil((float)fqr.n_chars*0.7);
    auto max_range = (size_t)(10 * m_k_size * fqr.n_chars * log(m_k_size * fqr.n_chars)); //minimize the pbb of collision

    getCandidatePrimes(min_range, max_range);

    /*getSuitableValues(min_range, max_range);
    for(size_t i=0;i<N_PRIMES;i++){
       std::cout<<m_q[i]<<" "<<d[i]<<" "<<m_inv_d[i]<<" "<<m_h<<" "<<yi[i]<<" "<<inv_yi[i]<<std::endl;
       //std::cout<<(size_t)(static_cast<__int128_t>(yi[i]) * static_cast<__int128_t>(inv_yi[i]))%m_q[i]<<std::endl;
    }*/
    initFingerPrints();
    for(size_t i=0;i<N_PRIMES;i++){
       std::cout<<m_q[i]<<"\t"<<d[i]<<"\t"<<m_h[i]<<std::endl;
    }
}

void DNAFingerPrint::getCandidatePrimes(size_t min_range, size_t max_range) {

    size_t max_val, tmp_pow;

    for(size_t i=0;i<N_PRIMES;i++) {
        bool prime_fits=false;
        while (!prime_fits) {
            m_q[i] = PrimeGenerator::getRandomPrime(min_range, max_range); //random prime in the range [min_range, max_range]
            d[i] = (size_t)ModOperations::findSmallestPrimitiveRoot((int)m_q[i]); //primitive root of q
            m_h[i] = ModOperations::mod_pow(d[i], m_k_size, m_q[i]); // d^{kmer_size} % q for joining fingers
            m_inv_d[i] = ModOperations::mod_pow(d[i], m_q[i] - 2, m_q[i]); // multiplicative inverse of d under % q
            max_val = std::max(d[i], m_inv_d[i]); // max*m_q must fit a machine word
            if(!Utils::isOverflowInMul(m_q[i], max_val)) {
                prime_fits = true;
            }
        }
        //pre compute (symbol*sigma^{k-1})%q
        tmp_pow = ModOperations::mod_pow(d[i], m_k_size - 1, m_q[i]); // d^{kmer_size-1} % q
        for (size_t j = 0; j < DNAAlphabet::sigma; j++) {
            m_precom_vals[i][j] = (j * tmp_pow) % m_q[i];
        }
    }
}

void DNAFingerPrint::initFingerPrints() {

    std::stack<unsigned char> rev_comp;
    unsigned char pos_symbol, neg_symbol;
    size_t g, l;

    for(size_t i=0;i<N_PRIMES;i++) {
        m_pos_fp[i] = 0;
        m_neg_fp[i] = 0;
    }

    for(size_t i =0;i<m_k_size;i++){
        pos_symbol = fqr.curr_symbol;
        neg_symbol = DNAAlphabet::comp2rev[pos_symbol];
        for(size_t j=0;j<N_PRIMES;j++) {
            m_pos_fp[j] = (d[j] * m_pos_fp[j] + pos_symbol);
            m_pos_fp[j] %= m_q[j];
        }
        m_pos_queue.push(pos_symbol);
        m_neg_queue.push(neg_symbol);
        rev_comp.push(neg_symbol);
        fqr.nextSymbolInRead();
    }

    while(!rev_comp.empty()){
        neg_symbol = rev_comp.top();
        for(size_t j=0;j<N_PRIMES;j++) {
            m_neg_fp[j] = (d[j] * m_neg_fp[j] + neg_symbol);
            m_neg_fp[j] %= m_q[j];
        }
        rev_comp.pop();
    }

    //join finger values as f(A+B) where A is the finger of direct and B is the finger of reverse
    for(size_t i=0;i<N_PRIMES;i++){
        g = std::max(m_pos_fp[i], m_neg_fp[i]);
        l = std::min(m_pos_fp[i], m_neg_fp[i]);
        m_curr_fingers[i] = l * m_h[i] + g;
        if(m_curr_fingers[i] >= m_q[i]) m_curr_fingers[i]%=m_q[i];
    }

    m_n_printed_kmers++;

    /*colls_checker.insert({m_curr_finger, {getCurrentKmer(false), {m_pos_fp[0], m_pos_fp[1], m_pos_fp[2],
                                                                  m_neg_fp[0], m_neg_fp[1], m_neg_fp[2]}}});*/
}

std::string DNAFingerPrint::getCurrentKmer(bool reverse_comp) {

    std::queue<unsigned char> tmp_queue;
    std::string tmp_string("", m_k_size);

    if(reverse_comp){
        tmp_queue = m_neg_queue;
        for(size_t j=m_k_size;j--;){
           tmp_string[j] = DNAAlphabet::comp2char[tmp_queue.front()];
           tmp_queue.pop();
        }
    }else{
        tmp_queue = m_pos_queue;
        for(size_t i=0;i<m_k_size;i++){
            tmp_string[i] = DNAAlphabet::comp2char[tmp_queue.front()];
            tmp_queue.pop();
        }
    }
    return tmp_string;
}

