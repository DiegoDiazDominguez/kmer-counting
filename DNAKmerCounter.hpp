//
// Created by diediaz on 17-07-18.
//

#ifndef STRINGFINGERPRINTGENERATOR_DNAKMERCOUNTER_HPP
#define STRINGFINGERPRINTGENERATOR_DNAKMERCOUNTER_HPP

#include <iostream>
#include "DNAFingerPrint.hpp"
#include "Utils.hpp"


class DNAKmerCounter {

private:

    struct HashedInfo{
       std::vector<uint32_t> freq;
       std::vector<std::string> kmer;
       std::vector<size_t> validators;
    };

    DNAFingerPrint dfp;
    size_t kmer_size;
    int app_dolls;

public:
    DNAKmerCounter(std::string file, size_t k_size, size_t app_dollars);
    void countDNAKmers();
};


#endif //STRINGFINGERPRINTGENERATOR_DNAKMERCOUNTER_HPP
