//
// Created by Diego Diaz on 3/23/18.
//
#include "DNAAlphabet.hpp"

const size_t DNAAlphabet::sigma = 6;
const unsigned char DNAAlphabet::comp2char[6] = {0, 36, 65, 67, 71, 84};
const unsigned char DNAAlphabet::comp2rev[6] = {0, 1, 5, 4, 3, 2};
const unsigned char DNAAlphabet::char2comp[117] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                          0, 0, 0, 0, 0, 2, 0, 3, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0,
                                          0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 3,
                                          0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5};

DNAAlphabet::size_type DNAAlphabet::serialize(std::ostream &out, structure_tree_node *v, std::string name)const
{
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;
    written_bytes += m_C.serialize(out, child, "C");
    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

void DNAAlphabet::load(std::istream &in)
{
    m_C.load(in);
}

void DNAAlphabet::swap(DNAAlphabet &in) {
    m_C.swap(in.m_C);
}

DNAAlphabet::symbol_type DNAAlphabet::rank2char(size_t rank) const {

    //TODO change this, it is awful
    if(rank<=m_C[2]){
       return 1;
    }else if(m_C[2]<rank && rank <=m_C[3]){
       return 2;
    }else if(m_C[3]<rank && rank <=m_C[4]){
       return 3;
    }else if(m_C[4]<rank && rank <=m_C[5]){
       return 4;
    }else{
       return 5;
    }
}
