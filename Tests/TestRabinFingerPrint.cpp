//
// Created by diediaz on 17-07-18.
//

#include <gtest/gtest.h>
#include "../DNAFingerPrint.hpp"


TEST(DNAFingerPrintTestsSuite, DNAFingerprintCorrectness) {

    std::string file = "/home/diediaz/CLionProjects/kmer-counting/bin/test.fasta";
    size_t kmer_size = 5;
    size_t app_dollars =0;
    size_t pos;
    int res[2]={-1,-1};
    size_t min, max;

    for(size_t i=0;i<5000;i++) {
        DNAFingerPrint dfp(file, kmer_size, app_dollars);
        res[0]=-1;
        res[1]=-1;
        pos=0;
        while (!dfp.isFinished()) {
            if( (pos==0) | (pos==18) | (pos==41) | (pos==53) | (pos==61)) {
                min = std::min(dfp.pos_fp, dfp.neg_fp);
                max = std::max(dfp.pos_fp, dfp.neg_fp);
                if(res[0]==-1 && res[1]==-1){
                   res[0] = (int)min;
                   res[1] = (int)max;
                }else{
                    ASSERT_EQ(min, res[0]);
                    ASSERT_EQ(max, res[1]);
                }
            }
            dfp.nextValidKmer();
            pos++;
        }
        ASSERT_EQ(1,1);
    }
}

TEST(DNAFingerPrintTestsSuite, Test2) {
    ASSERT_EQ(1,1);
}

TEST(DNAFingerPrintTestsSuite, Test3) {
    ASSERT_EQ(1,1);
}

int main(int argc, char** argv){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
