//
// Created by Diego Diaz on 5/30/18.
//

#include <iostream>
#include <ctime>
#include <stxxl.h>
#include "PrimeGenerator.hpp"
#include "DNAAlphabet.hpp"
#include "FastQReader.hpp"
#include "DNAFingerPrint.hpp"
#include "Prng.hpp"
#include "DNAKmerCounter.hpp"
#include <tsl/hopscotch_map.h>

#define DATA_NODE_BLOCK_SIZE (4096)
#define DATA_LEAF_BLOCK_SIZE (4096)

int main(){
    //TODO the number of appended dollars has to be < kmer_size
    //TODO assert if file exists
    std::string file = "/Users/diegodiaz/CLionProjects/kmer-counting/bin/ecoli_10x.fq";
    //TODO assert that kmer size is less than min read in the file
    size_t kmer_size = 5;
    size_t app_dollars =0;

    //size_t test =0;
    typedef stxxl::map<DNAFingerPrint::KKey, uint32_t, DNAFingerPrint::KGe, DATA_NODE_BLOCK_SIZE, DATA_LEAF_BLOCK_SIZE> map_type;
    map_type kmers_map((map_type::node_block_type::raw_size) * 3, (map_type::leaf_block_type::raw_size) * 3);

    //tsl::hopscotch_map<DNAFingerPrint::KKey, std::pair<size_t, std::string>, DNAFingerPrint::KHasher, DNAFingerPrint::KEq> kmers_map;

    clock_t begin = clock();
    DNAFingerPrint dfp(file, kmer_size, app_dollars);
    DNAFingerPrint::KKey kk;
    while(!dfp.isFinished()){
        kk = dfp.getKey();
        std::cout<<std::is_pod<DNAFingerPrint::KKey>::value<<std::endl;
        if(kmers_map.count(kk)==0){
           kmers_map.insert({kk, 1});
        }else{
           kmers_map[kk]++;
        }
        /*if(test==0 | test==18 | test==41 | test==53 | test==61) {
            std::cout<<dfp.curr_finger[0]<<"\t"<<dfp.curr_finger[1]<<"\t"<<dfp.curr_finger[2]<<"\t"<<dfp.getCurrentKmer(false)<<"\t"<<dfp.getCurrentKmer(true)<<"\t";
            for(size_t j=0;j<3;j++){
               std::cout<<dfp.pos_fp[j]<<"\t";
            }
            for(size_t j=0;j<3;j++){
                std::cout<<dfp.neg_fp[j]<<"\t";
            }
            std::cout<<""<<std::endl;
        }*/
        dfp.nextValidKmer();
    }

    /*for(const auto& key_value : kmers_map) {
        std::cout << "{" << key_value.first << ", " << key_value.second.first << ", "<<key_value.second.second<<"}" << std::endl;
    }*/


    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout<<elapsed_secs<<std::endl;
    std::cout<<"number of collisions: "<<dfp.n_colls<<std::endl;
/*
    DNAKmerCounter dkc(file, kmer_size, app_dollars);
    clock_t begin = clock();
    dkc.countDNAKmers();
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout<<elapsed_secs<<std::endl;
    */
}

