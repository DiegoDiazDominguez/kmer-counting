//
// Created by diediaz on 12-07-18.
//

#ifndef STRINGFINGERPRINTGENERATOR_DNAFINGERPRINT_HPP
#define STRINGFINGERPRINTGENERATOR_DNAFINGERPRINT_HPP
#include <iostream>
#include <queue>
#include "FastQReader.hpp"
#include "PrimeGenerator.hpp"
#include "ModOperations.hpp"
#include "Utils.hpp"

class DNAFingerPrint {
private:
    static const size_t N_PRIMES=3;
    static const size_t SIGMA=6;
    size_t m_k_size; //kmer size
    size_t m_curr_read_length;
    size_t m_tot_kmers;
    size_t m_n_printed_kmers;
    int m_last_kmer_index; //index of the last symbol in the kmer
    int m_dollar_diff;
    int m_curr_read_kmer;
    std::queue<unsigned char> m_pos_queue;
    std::queue<unsigned char> m_neg_queue;
    FastQReader fqr;
    //values involved in the fingerprints
    size_t m_precom_vals[N_PRIMES][SIGMA];
    size_t d[N_PRIMES];
    size_t m_q[N_PRIMES]; // random prime numbers
    size_t m_pos_fp[N_PRIMES]; // fingerprints of the original sequence
    size_t m_neg_fp[N_PRIMES]; // fingerprints of the reverse complement
    size_t m_h[N_PRIMES]; //d^(k_size-1) % q
    size_t m_inv_d[N_PRIMES]; //inverse multiplicative of d under modulo q
    size_t m_curr_fingers[N_PRIMES];

    struct KmerKey{
        size_t finger_a;
        size_t finger_b;
        size_t finger_c;
        inline bool operator==(const KmerKey &kk) const {
            return ((kk.finger_a == finger_a) &&
                    (kk.finger_b == finger_b) &&
                    (kk.finger_c == finger_c));
        }
        inline bool operator!=(const KmerKey &kk) const {
           return ((kk.finger_a != finger_a) &&
                    (kk.finger_b != finger_b) &&
                    (kk.finger_c != finger_c));
        }
    };

    struct DNAKmerHasher{
        inline std::size_t operator()(const KmerKey &kk) const {
            size_t seed = 0;
            Utils::hash_combine(seed, kk.finger_a);
            Utils::hash_combine(seed, kk.finger_b);
            Utils::hash_combine(seed, kk.finger_c);
            return seed;
        }
    };

    struct equal_KmerKey{
        inline bool operator()(const KmerKey &kk_1, const KmerKey &kk_2) const{
            return ((kk_1.finger_a == kk_2.finger_a) &&
                    (kk_1.finger_b == kk_2.finger_b) &&
                    (kk_1.finger_c == kk_2.finger_c));
        }
    };

    struct ComparatorGreater{
        inline bool operator()(const KmerKey &kk_1, const KmerKey &kk_2) const{

            if(kk_1.finger_a > kk_2.finger_a){
                return true;
            }else if((kk_1.finger_a == kk_2.finger_a) &&
                     (kk_1.finger_b > kk_2.finger_b)){
                return true;
            }else{
                return ((kk_1.finger_a == kk_2.finger_a) &&
                        (kk_1.finger_b == kk_2.finger_b) &&
                        (kk_1.finger_c > kk_2.finger_c));
            };
        }
        inline static KmerKey max_value(){
            size_t max_value = std::numeric_limits<size_t>::max();
            KmerKey kk = {max_value, max_value, max_value};
            return kk;
        }
    };

public:
    typedef KmerKey KKey;
    typedef DNAKmerHasher KHasher;
    typedef equal_KmerKey KEq;
    typedef ComparatorGreater KGe;
public:
    const size_t &curr_read_length = m_curr_read_length;
    const int &curr_read_kmer = m_curr_read_kmer;
    const size_t &tot_kmers = m_tot_kmers;

    //!just for testing collisions
    std::map<size_t, std::pair<std::string, std::vector<size_t>>> colls_checker;
    size_t n_colls=0;
private:
    friend std::ostream& operator <<(std::ostream &o, const KmerKey &kk) {
        o<< kk.finger_a <<"\t"<<kk.finger_b<<"\t"<<kk.finger_c;
        return o;
    }
    void initFingerPrints();
    void getCandidatePrimes(size_t min_range, size_t max_range);
    inline void nextFingerPrints(){
        unsigned char pos_curr_symbol, pos_first_symbol;
        unsigned char neg_curr_symbol, neg_first_symbol;
        size_t g,l;

        //positive kmer
        pos_curr_symbol = fqr.curr_symbol;
        pos_first_symbol = m_pos_queue.front();

        //negative kmer (reverse complement)
        neg_curr_symbol = DNAAlphabet::comp2rev[pos_curr_symbol];
        neg_first_symbol = m_neg_queue.front();

        for(size_t j=0;j<N_PRIMES;j++) {
            m_pos_fp[j] = d[j] * (m_pos_fp[j] + m_q[j] - m_precom_vals[j][pos_first_symbol]) + pos_curr_symbol;
            if (m_pos_fp[j] >= m_q[j]) m_pos_fp[j] %= m_q[j];

            m_neg_fp[j] = (m_neg_fp[j] + m_q[j] - neg_first_symbol) * m_inv_d[j] + m_precom_vals[j][neg_curr_symbol];
            if (m_neg_fp[j] >= m_q[j])m_neg_fp[j] %= m_q[j];
        }

        m_pos_queue.pop();
        m_pos_queue.push(pos_curr_symbol);

        m_neg_queue.pop();
        m_neg_queue.push(neg_curr_symbol);

        //join finger values as f(A+B) where A is the finger of direct and B is the finger of reverse
        for(size_t i=0;i<N_PRIMES;i++){
            g = std::max(m_pos_fp[i], m_neg_fp[i]);
            l = std::min(m_pos_fp[i], m_neg_fp[i]);
            m_curr_fingers[i] = l * m_h[i] + g;
            if(m_curr_fingers[i] >= m_q[i]) m_curr_fingers[i]%=m_q[i];
        }

        //TODO just for fixing
                /*
        if(colls_checker.count(m_curr_fingers[0])==0){
            colls_checker.insert({m_curr_fingers[0], {getCurrentKmer(false), {m_pos_fp[0], m_pos_fp[1], m_pos_fp[2],
                                                                              m_neg_fp[0], m_neg_fp[1], m_neg_fp[2]}}});
        }else{
            if(colls_checker[m_curr_fingers[0]].first!= getCurrentKmer(false) &&
               colls_checker[m_curr_fingers[0]].first!= getCurrentKmer(true)){
               std::vector<size_t> tmp = colls_checker[m_curr_fingers[0]].second;
               std::cout<<m_curr_fingers[0]<<" | ";
               std::cout<<m_pos_fp[0]<<" "<<m_pos_fp[1]<<" "<<m_pos_fp[2]<<" | ";
               std::cout<<m_neg_fp[0]<<" "<<m_neg_fp[1]<<" "<<m_neg_fp[2]<<" | ";
               std::cout<<colls_checker[m_curr_fingers[0]].first<<" | "<<getCurrentKmer(false)<<" "<<getCurrentKmer(true)<<" ";
               for(size_t i=0;i<6;i++){
                  std::cout<<tmp[i]<<" ";
               }
               std::cout<<""<<std::endl;
               n_colls++;
            }
        }
        std::queue<unsigned char> tmp_queue=m_pos_queue;
        std::stack<unsigned char> tmp_stack;
        while(!tmp_queue.empty()){
           tmp_stack.push(tmp_queue.front());
           tmp_queue.pop();
        }

        size_t k=0;
        for(size_t i=0;i<m_k_size;i++){
            k = d*k + tmp_stack.top();
            tmp_stack.pop();
        }
        k %=m_q;
        std::cout<<"\t\t"<<k<<std::endl;*/

        // update kmer info
        if(fqr.app_s==0 && fqr.curr_pos==0){
            m_curr_read_kmer = 1 - m_dollar_diff;
        }else{
            m_curr_read_kmer++;
        }

        if(m_curr_read_kmer==0) m_curr_read_length = fqr.curr_read_len + 2*fqr.append_dollars;
        m_last_kmer_index = fqr.curr_pos;

        // next symbol in fastx stream
        fqr.nextSymbolInRead();
    };

public:
    DNAFingerPrint(std::string &file, size_t kmer_size, size_t app_dollars);
    inline void nextValidKmer(){
        nextFingerPrints();
        if(m_last_kmer_index==0){

            for(int j=0;j<(m_dollar_diff-1);j++){
                nextFingerPrints();
            }
        }
        m_n_printed_kmers++;
    };

    inline KKey getKey(){
        KKey k = {m_curr_fingers[0], m_curr_fingers[1], m_curr_fingers[2]};
        return k;
    }

    inline bool isFinished(){
        return m_n_printed_kmers>m_tot_kmers;
    };
    std::string getCurrentKmer(bool reverse_comp);
};


#endif //STRINGFINGERPRINTGENERATOR_DNAFINGERPRINT_HPP
