//
// Created by diediaz on 17-07-18.
//

#include "DNAKmerCounter.hpp"
#include <tsl/hopscotch_map.h>

DNAKmerCounter::DNAKmerCounter(std::string file, size_t k_size, size_t app_dollars): dfp(file, k_size, app_dollars) {
   kmer_size = k_size;
   app_dolls = (int)app_dollars;
}

void DNAKmerCounter::countDNAKmers() {

   /*size_t key, rev_key;
   bool is_key_present, is_rev_key_present, key_is_rev_comp;
   int last_kmer_pos;*/

   //pair<rev_key, freq>
   tsl::hopscotch_map<DNAFingerPrint::KKey, HashedInfo, DNAFingerPrint::KHasher, DNAFingerPrint::KEq> kmers_map;
   //spp::sparse_hash_map<size_t, std::pair<size_t, size_t>> kmers_map;
   size_t collision=0;
   while(!dfp.isFinished()){
       /*
       if(kmers_map.count(dfp.curr_pos_finger)!=0){
           HashedInfo hi ={{1},{""}, {dfp.pos_fp[0]}};
           kmers_map.insert({dfp.curr_pos_finger,hi});
       }else{
           std::vector<size_t> tmp = kmers_map[dfp.curr_pos_finger].validators;
           bool is_coll = true;
           for (size_t i=0;i<tmp.size();i++) {
               if(tmp[i] == dfp.pos_fp[0]){
                  kmers_map[dfp.curr_pos_finger].freq[i]++;
                  is_coll = false;
                  break;
               }
           }
           //collision: very expensive 😔
           if(is_coll){
               collision++;
               kmers_map[dfp.curr_pos_finger].kmer.emplace_back("");
               kmers_map[dfp.curr_pos_finger].freq.push_back(1);
               kmers_map[dfp.curr_pos_finger].validators.push_back(dfp.pos_fp[0]);
           }
       }
       last_kmer_pos = (int)(dfp.curr_read_length-kmer_size);

       if(dfp.curr_read_kmer< app_dolls ||
          (dfp.pos_fp<=dfp.neg_fp && (dfp.curr_read_kmer <= (last_kmer_pos - app_dolls + 1)))){
           key = dfp.pos_fp;
           rev_key = dfp.neg_fp;
           key_is_rev_comp = false;
       }else{
           key = dfp.neg_fp;
           rev_key = dfp.pos_fp;
           key_is_rev_comp = true;
       }

       is_key_present = kmers_map.find(key)!=kmers_map.end();
       is_rev_key_present = kmers_map.find(rev_key)!=kmers_map.end();

       //the first time kmer is seen
       if(!is_key_present && !is_rev_key_present){ //this kmer is seen for the first time
           kmers_map.insert({key, {rev_key, 0}});
       }else if(is_key_present && !is_rev_key_present){//this kmer was seen before
            if(kmers_map[key].first==rev_key){
                kmers_map[key].second++;
            }else{
                //std::cout << dfp.pos_fp << "\t\t" << dfp.neg_fp << "\t\t" << dfp.getCurrentKmer(false) << "\t\t"
                //          << dfp.getCurrentKmer(true) <<std::endl;
                colls++;
                //there is a collision: the first time key was seen, it was paired with another rev_key
                //std::cout<<"collision 1"<<std::endl;
            }
       }
       //collision: rev_key was also paired with another key'
       else if(!is_key_present){
           colls++;
           //std::cout<<"collision 2"<<std::endl;
           //std::cout << dfp.pos_fp << "\t\t" << dfp.neg_fp <<"\t\t" << dfp.getCurrentKmer(false) << "\t\t"
           //          << dfp.getCurrentKmer(true) <<std::endl;
       }*/
       dfp.nextValidKmer();
   }
    std::cout<<"number of colls: "<<collision<<std::endl;
}
